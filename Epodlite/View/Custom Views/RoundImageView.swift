//
//  RoundImageView.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-16.
//


import Foundation
import UIKit
import Foundation
@IBDesignable class RoundImageView: UIImageView {
    let darkShadow = CALayer()
    let lightShadow = CALayer()
    func setshadow() {
    darkShadow.frame = self.bounds
    darkShadow.cornerRadius = 15
    darkShadow.backgroundColor = UIColor.white.cgColor
    darkShadow.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    darkShadow.shadowOffset = CGSize(width: 10, height: 10)
    darkShadow.shadowOpacity = 1
    darkShadow.shadowRadius = 15
    self.layer.insertSublayer(darkShadow, at: 0)
    lightShadow.frame = self.bounds
    lightShadow.cornerRadius = 15
        lightShadow.backgroundColor = UIColor.white.cgColor
    lightShadow.shadowColor = UIColor.white.withAlphaComponent(0.9).cgColor
    lightShadow.shadowOffset = CGSize(width: -5, height: -5)
    lightShadow.shadowOpacity = 1
    lightShadow.shadowRadius = 15
    
    self.layer.insertSublayer(lightShadow, at: 0)
      
    }
    
 
    override func layoutSubviews() {
        super.layoutSubviews()
    
        self.layer.borderWidth = 0
        self.layer.borderColor = self.tintColor.cgColor
        self.layer.cornerRadius = self.frame.height / 2
    
    }

}

