//
//  TrackDateCell.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-06-30.
//

import UIKit

class TrackDateCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dataTabelView: UITableView!
    var travelHistory : [TravelHistory]?
    override func awakeFromNib() {
        super.awakeFromNib()
        dataTabelView.delegate = self
        dataTabelView.dataSource = self
        dataTabelView.register(UINib(nibName: reuseNibIdentifier.trackDataCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.trackDataCell)
        dataTabelView.backgroundColor = .clear
        dataTabelView.isOpaque = true
        dataTabelView.backgroundView = nil
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TrackDateCell: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dataTabelView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.trackDataCell) as! TrackDataCell
        if let x = travelHistory?[indexPath.row]
        {
            cell.statusLabel.text = x.status
            cell.timeLabel.text = x.time
        }
       // print(x)
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let c = travelHistory?.count ?? 0
        let h = (c * 80)
       // tableviewHeight.constant = CGFloat(h)
        print("row")
        print(h)
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let c = travelHistory?.count ?? 0
        let h = (c * 100)
      //  tableviewHeight.constant = CGFloat(h)
        print("row")
        print(h)
        return travelHistory?.count ?? 0
    }

    
    
}

