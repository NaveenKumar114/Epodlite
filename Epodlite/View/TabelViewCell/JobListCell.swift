//
//  JobListCell.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-08.
//

import UIKit

class JobListCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var jobNumber: UILabel!
    @IBOutlet weak var trackingNumber: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var toDay: UILabel!
    @IBOutlet weak var toDate: UILabel!
    @IBOutlet weak var fromDate: UILabel!
    @IBOutlet weak var fromDay: UILabel!
    @IBOutlet weak var firstStatusImage: UIImageView!
    @IBOutlet weak var secondStatusImage: UIImageView!
    @IBOutlet weak var thirdStatusImage: UIImageView!
    @IBOutlet weak var fourthStatusImage: UIImageView!
    @IBOutlet weak var statusImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainView.cornerRadius = 5
     //   self.mainView.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
