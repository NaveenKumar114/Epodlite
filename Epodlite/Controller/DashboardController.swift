//
//  DashboardController.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-02.
//client login name
//company name
//profile

import UIKit
import MaterialComponents.MaterialBottomNavigation
import SwiftyGif
class DashboardController: UIViewController, UITextFieldDelegate , docketScannerDelegate, MDCBottomNavigationBarDelegate {
    func scanSuccess(searchString: String) {
        trackingNumberTextField.text = searchString
        trackButtonPressed(trackingNumberTextField as Any)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
     
        return false

    }
    override func viewWillDisappear(_ animated: Bool) {
      //  super.viewWillDisappear(true)
      
       // self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false

   
    }
    override func viewWillAppear(_ animated: Bool) {
     //   super.viewWillAppear(true)
     //   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
    //    self.navigationController?.navigationBar.shadowImage = UIImage()
     //   self.navigationController?.navigationBar.isTranslucent = true
     //   self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.isNavigationBarHidden = true

    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    var isCollapsed = false

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var referenceVIew: UIView!
    @IBOutlet weak var docketView: UIView!
    @IBOutlet weak var jobNumberView: UIView!
    @IBOutlet weak var trackingNumberTextField: UITextField!
    @IBOutlet weak var searchNumberLabel: UILabel!
    @IBOutlet weak var cameraImageVIew: UIImageView!
    @IBOutlet weak var navigationBar: MDCBottomNavigationBar!
    @IBOutlet weak var jobListTableView: UITableView!
    @IBOutlet weak var jobListView: UIView!
    @IBOutlet weak var subscriberImage: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var subscriberName: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collapseVIew: RoundAndElevate!
    @IBOutlet weak var searchTypesView: UIView!
    var refreshControl = UIRefreshControl()

    var searchViews = [UIView]()
    let child = SpinnerViewController()
    var navigationBarItems = [UITabBarItem]()
    var selectedType : shipmentType = .all
    var joblist : TrackingJSON?
    var selectedSearch = 1 // 1 - docket 2- jobnumber 3 - Reference check this in api too
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(collapseTracking))
        collapseVIew.addGestureRecognizer(gesture2)
        trackingNumberTextField.delegate = self
        jobListTableView.delegate = self
        jobListTableView.dataSource = self
        jobListTableView.register(UINib(nibName: reuseNibIdentifier.jobListCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.jobListCell)
        jobListTableView.separatorColor = .clear
        jobListTableView.backgroundColor = ConstantsUsedInProject.dashboardBackground
        jobListTableView.isOpaque = false
        jobListTableView.backgroundView = nil
        searchViews = [docketView , jobNumberView , referenceVIew]
      setupSelectedSearch()
        setUpGestures()

        let loggedUsername = UserDefaults.standard.string(forKey: "\(userdefaultsKey.isLoggedIN)")
            if loggedUsername == "TRUE" {
                loginView.isHidden = true
                jobListView.isHidden = false
                clientName.text = UserDefaults.standard.string(forKey: "\(userdefaultsKey.clientName)")
                    subscriberName.text = UserDefaults.standard.string(forKey: "\(userdefaultsKey.subscriberName)")
                if let img =  UserDefaults.standard.string(forKey: "\(userdefaultsKey.subscriberImg)")
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)profile/\(img)")
                    print(urlStr)
                    let url = URL(string: urlStr)
                    
                    DispatchQueue.global().async {
                        
                       if let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                       {
                        DispatchQueue.main.async { [self] in
                            subscriberImage.image = UIImage(data: data)
                            subscriberImage.contentMode = .scaleAspectFill
                            subscriberImage.cornerRadius = subscriberImage.frame.width / 2
                            //profileVIew.layer.cornerRadius = 5
                          //  profileVIew.layer.cornerRadius = profileVIew.frame.width / 2
                           // profileVIew.layer.borderWidth = 5
                           // profileVIew.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
                           // profileVIew.elevate(elevation: 5)
                            //logoImageVIew.contentMode = .scaleAspectFit
                        }
                           
                        }
                    }
                }
                else
                {

                    subscriberImage.image = UIImage(named: "epodlite")
                    subscriberImage.contentMode = .scaleAspectFill
                    subscriberImage.cornerRadius = subscriberImage.frame.width / 2
                }
                refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
                refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
                jobListTableView.addSubview(refreshControl)

                loadJobList()
            }
        else
            {
                collapseVIew.isHidden = true

                subscriberImage.image = UIImage(named: "epodlite")
                subscriberImage.contentMode = .scaleAspectFill
                subscriberImage.cornerRadius = subscriberImage.frame.width / 2
                loginView.isHidden = false
                jobListView.isHidden = true
                logoutButton.isHidden = true
            }
       //trackingNumberTextField.text = "V8sIB4yM2WPc"
        setupNavigationBar()
        
    }
    @objc func collapseTracking(firstTIme : Bool)
    {
      
        if isCollapsed
        {
            //247
            rotate(by: .pi)
            view.layoutIfNeeded()
            contentViewHeight.constant = 251
            searchTypesView.isHidden = false
            UIView.animate(withDuration: 1.0, animations: {
                 self.view.layoutIfNeeded()
            })
        }
        else
        {
            rotate(by: -π)
            //self.rotate(by: -.pi)

            view.layoutIfNeeded()
            contentViewHeight.constant = 35

          
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
            } completion: { [self] _ in
       
                searchTypesView.isHidden = true


            }

        }
        isCollapsed.toggle()
    }
    var viewAngle: CGFloat = 0    // Right-side up to start
    let π = CGFloat.pi   // Swift allows special characters hold alt+p to use this, it allows for cleaner code
    /// Rotate trackingview icon
    /// - parameter by :  Angle to rotate
    
    func rotate(by angle: CGFloat) {

        for i in 0 ..< 4 {
            UIView.animate(withDuration: 0.4, delay: 0.125 * Double(i), options: .curveLinear, animations: {
                print(self.viewAngle)
                self.viewAngle += angle/4
                self.collapseVIew.transform = CGAffineTransform(rotationAngle: self.viewAngle)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }

    }
    @objc func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
    loadJobList()
    }
    func refreshSegmentControl()
    {
        //navigationBarItems.removeAll()
        if let x =  joblist
        {
            let collectingCount = Int(x.jobCounts?.collectingCount ?? "0") ?? 0
            let transitCount = Int(x.jobCounts?.transitCount ?? "0") ?? 0
            let add = collectingCount + transitCount
        navigationBarItems[0].title =  """
                ALL
                 (\(x.jobCounts?.allCount ?? "0"))
                """
            navigationBarItems[1].title = """
                    Booked
                     (\(x.jobCounts?.bookedCount ?? "0"))
                    """
        navigationBarItems[2].title = """
                In Transit
                 (\(String(add)))
                """

        navigationBarItems[3].title = """
                Delivered
                 (\(x.jobCounts?.deliveredCount ?? "0"))
                """

       

        }

    }
    func setupNavigationBar()
    {
        let allItem = UITabBarItem(
            title: """
                All
                (0)
                """,
            image: UIImage(named:"collections"),
            tag: 0)
        
        navigationBarItems.append(allItem)
        let booked = UITabBarItem(
            title: """
                Booked
                (0)
                """,
            
            image: UIImage(named:"book_black"),
            tag: 1)
        navigationBarItems.append(booked)

        let inTransitItem = UITabBarItem(
            title: """
                In Transit
                (0)
                """,
            image: UIImage(named:"intransit"),
            tag: 2)
        navigationBarItems.append(inTransitItem)
        let deliveredItem = UITabBarItem(
            title: """
                Delivered
                (0)
                """,
            
            image: UIImage(named:"bookmark_added"),
            tag: 3)
        navigationBarItems.append(deliveredItem)
        

       navigationBar.items = [allItem , booked , inTransitItem, deliveredItem ]
        navigationBar.selectedItem = allItem
        navigationBar.delegate = self
        navigationBar.selectedItemTintColor = ConstantsUsedInProject.appThemeColor
        navigationBar.selectedItemTitleColor = ConstantsUsedInProject.appThemeColor
        navigationBar.tintColor = .darkGray
        navigationBar.titlesNumberOfLines = 2
        navigationBar.titleVisibility = MDCBottomNavigationBarTitleVisibility.always
        navigationBar.elevation = ShadowElevation(0)
        navigationBar.itemsContentVerticalMargin = 5
    }
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
        if item.tag == 0
        {
            selectedType = .all
           
        }
        else
        if item.tag == 1
        {
            selectedType = .booked
           

        }
        
        else
        if item.tag == 2

        {
            selectedType = .intransit
          

        }
        if item.tag == 3

        {
            selectedType = .delivered
          

        }
        loadJobList()
    }
    func loadJobList()
    {
        var status = "All"
        switch selectedType {
        case .booked:
            status = "Booked"
        case .delivered:
            status = "Delivered"
        case .intransit:
            status = "In Transit"
        case .all:
            status = "All"

        }
        let clientID = UserDefaults.standard.string(forKey: userdefaultsKey.cliendid)
        let json: [String: Any] = ["client_id": "\(clientID!)",
                                   "type": "4",
                                   "number": "" ,
                                   "status" : "\(status)"]

        print(json)
        createSpinnerView()
        makePostCallJobList(json: json)
    }
    func setUpGestures()
    {
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(docketPressed))
        docketView.addGestureRecognizer(gesture1)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(jobNumberPressed))
        jobNumberView.addGestureRecognizer(gesture2)
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(referencePressed))
        referenceVIew.addGestureRecognizer(gesture3)
        let gesture4 = UITapGestureRecognizer(target: self, action: #selector(docketScannerPressed))
        cameraImageVIew.addGestureRecognizer(gesture4)
    }
    @objc func docketScannerPressed()
    {
        performSegue(withIdentifier: segueIdentifiers.dashboardToDocketScanner, sender: nil)
    }
   @objc  func referencePressed() {
        print("reference pressed")
        selectedSearch = 3
        setupSelectedSearch()
    searchNumberLabel.text = "Search Reference Number"
    }
    @objc func jobNumberPressed() {
        print("jobnumber pressed")
        selectedSearch = 2

        setupSelectedSearch()
        searchNumberLabel.text = "Search Job Number"

    }
    //0 - Reference 1 - docket 2- jobnumber
    @objc func docketPressed()
    { print("docket pressed")
        selectedSearch = 1
        searchNumberLabel.text = "Search Docket Number"

        setupSelectedSearch()
    }
    
   
    func setupSelectedSearch()
    {
        // 1 - docket 2- jobnumber 3 - Reference check this in api too
        cameraImageVIew.isHidden = true
        for n in 0 ... 2
        {
            if n + 1 == selectedSearch
            {
                
                searchViews[n].elevate(elevation: 10)
                searchViews[n].backgroundColor = ConstantsUsedInProject.projectLightGreen
                if n + 1 == 1
                {
                    cameraImageVIew.isHidden = false
                }
            }
            else
            {
                searchViews[n].elevate(elevation: 0)
                searchViews[n].backgroundColor = UIColor.white
            }
            
        }
        
    }
    
    @IBAction func trackButtonPressed(_ sender: Any) {
       // self.performSegue(withIdentifier: segueIdentifiers.dashboardToTracking, sender: nil)

        if trackingNumberTextField.text == ""
        {
            let alert = UIAlertController(title: "Dashboard", message: "Please enter a number", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let clientID = UserDefaults.standard.string(forKey: userdefaultsKey.cliendid)
            let json: [String: Any] = ["client_id": "0",
                                       "type": "\(selectedSearch)",
                                       "number": "\(trackingNumberTextField.text!)" ,
                                       "status" : "track"]

            //cliend id 0 if not logged in 
            print(json)
            createSpinnerView()
            makePostCallTrack(json: json)
        }
    }
    func makePostCallJobList(json : [String: Any] ) {
        let decoder = JSONDecoder()
      
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)tracking/tracking_by_number")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let jsonResponse = try? decoder.decode(TrackingJSON.self, from: data!)
                    let code_str = jsonResponse!.code
                    
                    DispatchQueue.main.async {
                        self.removeSpinnerView()

                        if code_str == 200 {
                           
                            self.joblist = jsonResponse
                            self.jobListTableView.reloadData()
                            self.refreshSegmentControl()

                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            self.joblist = jsonResponse
                            self.jobListTableView.reloadData()
                            
                           // self.refreshSegmentControl()
                            let alert = UIAlertController(title: "Dashboard", message: "\(jsonResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            //self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    @IBAction func logOutPressed(_ sender: Any) {
        
          
          let alert = UIAlertController(title: "LogOut", message: "Are You Sure You Want To Sign Out", preferredStyle: .alert)
          
          let ok = UIAlertAction(title: "OK", style: .default, handler: { [self] action in
              let defaults = UserDefaults.standard
              defaults.setValue("FALSE", forKey: "\(userdefaultsKey.isLoggedIN)")
              let storyboard = UIStoryboard(name: "Login", bundle: nil)

              let dashboard1 = storyboard.instantiateViewController(identifier: "login")
              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard1)
          })
          alert.addAction(ok)
          let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
          })
          alert.addAction(cancel)
          DispatchQueue.main.async(execute: {
              self.present(alert, animated: true)
          })
    }
    func makePostCallTrack(json : [String: Any] ) {
        let decoder = JSONDecoder()
      
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)tracking/tracking_by_number")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let jsonResponse = try? decoder.decode(TrackingJSON.self, from: data!)
                    let code_str = jsonResponse!.code
                    
                    DispatchQueue.main.async {
                        self.removeSpinnerView()

                        if code_str == 200 {
                           // print(jsonResponse)
                            let x = jsonResponse?.jobList![0]
                            self.performSegue(withIdentifier: segueIdentifiers.dashboardToTracking, sender: x)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Dashboard", message: "\(jsonResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.dashboardToTracking
        {
          
            let x = sender as! JobList
            let vc = segue.destination as! TrackViewController
            vc.trackingData = x
            print(x)
            trackingNumberTextField.text = ""
            
        }
        if segue.identifier == segueIdentifiers.dashboardToDocketScanner
        {
            let vc = segue.destination as! DocketScannerController
            vc.delegate = self

        }
        
    }

}

extension DashboardController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      

        let cell = jobListTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.jobListCell) as! JobListCell
        if let x = joblist?.jobList?[indexPath.row]
        {
            
            cell.jobNumber.text = x.clientReferenceNo
            cell.trackingNumber.text = x.docketNumber?.uppercased()
            cell.status.text = x.trackStatus
            switch x.trackStatus?.uppercased() {
            case "BOOKED":
                cell.firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.secondStatusImage.tintColor = .lightGray
                cell.thirdStatusImage.tintColor = .lightGray
                cell.fourthStatusImage.tintColor = .lightGray
            case "COLLECTING" , "PICKED UP":
                cell.firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.thirdStatusImage.tintColor = .lightGray
                cell.fourthStatusImage.tintColor = .lightGray
            case "IN TRANSIT":
                cell.firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.thirdStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.fourthStatusImage.tintColor = .lightGray
            case "DELIVERED":
                cell.firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.thirdStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
                cell.fourthStatusImage.image = UIImage(named: "last_complete")
            default:
                print(x.trackStatus as Any)
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let etadate = (dateFormatter.date(from: x.jobEtaDate!))!
            let etddate = (dateFormatter.date(from: x.jobEtdDate!))!

            cell.fromDate.text = convertDateFormater(x.jobEtdDate!)
            cell.toDate.text = convertDateFormater(x.jobEtaDate!) 
            cell.fromDay.text = etddate.dayOfWeek()
            cell.toDay.text = etadate.dayOfWeek()
            var transportTypeGifImage = "air_center" // default if none match
            switch x.modeType {
            case "Air":
                transportTypeGifImage = "air_center"
            case "Road":
                transportTypeGifImage = "trucking_center"
            case "Hand Carry":
                transportTypeGifImage = "handcarry_center"
            case "Sea":
                transportTypeGifImage = "sea_center"
            case "Courier":
                transportTypeGifImage = "courier_center"
            case "Hot Shot":
                transportTypeGifImage = "hotshot_center"
            default:
                print("err")
            }
            let transportImage =  UIImage(named: transportTypeGifImage)?.withTintColor(.white)
            cell.statusImage.image = transportImage?.withTintColor(.white)
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        

        return 105
       

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if joblist?.jobList?.count ?? 0 == 0 {
              self.jobListTableView.setEmptyMessage("No Job Found")
          } else {
            self.jobListTableView.restore()
          }
        return joblist?.jobList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let x = joblist?.jobList?[indexPath.row]
        jobListTableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: segueIdentifiers.dashboardToTracking, sender: x)
        
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: date!)
        
    }

}



enum shipmentType {
    case booked
    case intransit
    case delivered
    case all
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}



extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}
