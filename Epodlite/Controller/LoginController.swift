//
//  LoginController.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-01.
//

import UIKit
import CoreTelephony
import Firebase
import FirebaseAuth
class LoginController: UIViewController, UITextFieldDelegate {
    override func viewWillDisappear(_ animated: Bool) {
      //  super.viewWillDisappear(true)
      
       // self.navigationController?.navigationBar.isTranslucent = false
     //   self.navigationController?.isNavigationBarHidden = false

   
    }
    override func viewWillAppear(_ animated: Bool) {
     //   super.viewWillAppear(true)
     //   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
    //    self.navigationController?.navigationBar.shadowImage = UIImage()
     //   self.navigationController?.navigationBar.isTranslucent = true
     //   self.navigationController?.view.backgroundColor = .clear
        //self.navigationController?.isNavigationBarHidden = true

    }
    var tokenID = "x"

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pasword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        pasword.isSecureTextEntry = true
        email.delegate = self
        pasword.delegate = self
       // email.text = "admin@gfs.com"
        //  pasword.text = "123456"

        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            self.tokenID = token
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
     
        return false

    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        makePostCallLogin(email: email.text!, password: pasword.text!)
    }
    func makePostCallLogin(email : String , password : String) {
        let decoder = JSONDecoder()
      //  email=\(email.text!)&password=\(pasword.text!)&tokenid=\(tokenID)&type=1"
        let json: [String: Any] = ["email": "\(email)",
                                   "password": "\(password)", "tokenid" : "\(tokenID)" , "type" : "1"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)Client/client_login")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(EmailJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(loginBaseResponse?.client?.clientID ?? "", forKey: "\(userdefaultsKey.cliendid)")
                            defaults.setValue("TRUE", forKey: "\(userdefaultsKey.isLoggedIN)")
                            defaults.setValue(loginBaseResponse?.client?.clientName ?? "", forKey: "\(userdefaultsKey.clientName)")
                            defaults.setValue(loginBaseResponse?.subscriber?.companyName ?? "", forKey: "\(userdefaultsKey.subscriberName)")
                            defaults.setValue(loginBaseResponse?.subscriber?.profileImage ?? "", forKey: "\(userdefaultsKey.subscriberImg)")
                            let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                            let dashboard = storyboard2.instantiateViewController(identifier: "dash")

                            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "\(loginBaseResponse?.response ?? "Invalid Email & Password")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

 

}
