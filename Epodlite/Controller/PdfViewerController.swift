//
//  PdfViewerController.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-07.
//

import UIKit
import PDFKit
class PdfViewerController: UIViewController {

    @IBOutlet weak var viewContainingPdf: UIView!
    var pdfPath : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        let pdfView = PDFView()
        
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        viewContainingPdf.addSubview(pdfView)
        
        pdfView.leadingAnchor.constraint(equalTo: viewContainingPdf.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: viewContainingPdf.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: viewContainingPdf.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: viewContainingPdf.safeAreaLayoutGuide.bottomAnchor).isActive = true
        loadFileAsync(url: URL(string: "\(ConstantsUsedInProject.baseDocumentUrl)\(pdfPath!)")!) { (a, e) in
            DispatchQueue.main.async {
                print("ss")

            if a != nil
            {
                if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: a!)) {
                    pdfView.displayMode = .singlePageContinuous
                    pdfView.autoScales = true
                    pdfView.displayDirection = .vertical
                    pdfView.document = pdfDocument
                    self.pdfPath = a!
                }
                
            }
            else
            {
                print(e)
            }
            }
        }
        
        
    }
   
    func loadPDFAndShare(a : String){

               let fileManager = FileManager.default
        let documentoPath = a
       // let documentoPath = (fileManager.getDirectoryPath() as NSString).appendingPathComponent("documento.pdf")

               if fileManager.fileExists(atPath: documentoPath){
                    let documento = NSData(contentsOfFile: documentoPath)
                    let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView=self.view
                self.present(activityViewController, animated: true, completion: nil)
                }
                else {
                    print("document was not found")
                }
            }
    func load(URL: URL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data: Data!, response: URLResponse!, error: Error!) -> Void in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                //print(data)
                // This is your file-variable:
                // data
            }
            else {
                // Failure
                print("Failure: %@", error.localizedDescription);
            }
        })
        task.resume()
    }
    func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {

        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            completion(destinationUrl.path, nil)

        }
        else
        {
            print("s")
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
                                            {
                                                data, response, error in
                                                print("ss")

                                                if error == nil
                                                {
                                                    print("sss")

                                                    if let response = response as? HTTPURLResponse
                                                    {
                                                        print("ssss")

                                                        if response.statusCode == 200
                                                        {
                                                            print("sssss")

                                                            if let data = data
                                                            {
                                                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                                else
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                            }
                                                            else
                                                            {
                                                                completion(destinationUrl.path, error)
                                                            }
                                                        }
                                                        else
                                                        {
                                                            print("ssssss")
                                                            print(response.statusCode)
                                                        }
                                                    }
                                                }
                                                else
                                                {

                                                    completion(destinationUrl.path, error)
                                                }
                                            })
            task.resume()
        }
    }
    @IBAction func shareButtonPressed(_ sender: Any) {
        if pdfPath != nil
        {
            loadPDFAndShare(a: pdfPath!)
            
        }
        else
        {
            let alert = UIAlertController(title: "UsefulLink", message: "No file found", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
