//
//  TrackViewController.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-06-30.
//
import MapKit
import UIKit
import SwiftyGif
import curvyRoute
import IVBezierPathRenderer
class TrackViewController: UIViewController , MKMapViewDelegate {

    @IBOutlet weak var bottomButtonsView: UIView!
    @IBOutlet weak var trackingVIew: UIView!
    @IBOutlet weak var trackTavleVIew: UITableView!
    @IBOutlet weak var trackingVIewHeight: NSLayoutConstraint!
    var trackingData : JobList?
    @IBOutlet weak var collapeseView: RoundAndElevate!
    @IBOutlet weak var timeFirst: UILabel! // remove this if needed , no use
    @IBOutlet weak var statusLabelFirst: UILabel!
    @IBOutlet weak var dottedImageONe: UIImageView!
    @IBOutlet weak var gifOne: UIImageView!
    @IBOutlet weak var imageViewForGIf: UIImageView!
    @IBOutlet weak var gifThree: UIImageView!
    @IBOutlet weak var fromImage: UIImageView!
    @IBOutlet weak var toImage: UIImageView!
    @IBOutlet weak var bookedStatusLabel: UILabel! // order placed
    @IBOutlet weak var pickedUpStatusLabel: UILabel!
    @IBOutlet weak var inTransitStatusLabel: UILabel!
    @IBOutlet weak var deliveredStatusLabel: UILabel!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var fromName: UILabel!
    @IBOutlet weak var toName: UILabel!
    @IBOutlet weak var trackingNumber: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var noOfPackages: UILabel!
    @IBOutlet weak var shipmentType: UILabel!
    @IBOutlet weak var specialHandlingSection: UILabel!
    @IBOutlet weak var additionalServices: UILabel!
    @IBOutlet weak var signatureServices: UILabel!
    @IBOutlet weak var tersmOfDelivery: UILabel!
    @IBOutlet weak var cargoDelivery: UILabel!
    @IBOutlet weak var additionalInfo: UILabel!
    @IBOutlet weak var shipmentTypeUNderGIf: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var podView: UIView!
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var firstStatusImage: UIImageView!
    @IBOutlet weak var secondStatusImage: UIImageView!
    @IBOutlet weak var thirdStatusImage: UIImageView!
    @IBOutlet weak var fourthStatusImage: UIImageView!
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var podImageVIew: UIImageView!
    @IBOutlet weak var mapCollapseView: RoundAndElevate!
    @IBOutlet weak var jobNumber: UILabel!
    @IBOutlet weak var referenceNumber: UILabel!
    
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var statsuLabel: UILabel!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var gifFour: UIImageView!
    @IBOutlet weak var gifTwo: UIImageView!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var fromToVIew: UIView!
    @IBOutlet weak var fromToMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var arrivingByDate: UILabel!
    @IBOutlet weak var viewToDrawDottedLine: UIView!
    @IBOutlet weak var dottedImageView: UIImageView!
    @IBOutlet weak var bottomButtonHeight: NSLayoutConstraint!
    var gifViewArray = [UIImageView]()
    var roundImageArray = [UIImageView]()
    var isCollapsed = false
    var isCollapsedMapView = false
    var jobStatus = 0 //0 - booked , 1-pickedup , 2-intransit , 3-delivered
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var viewWithMapAndDetails: UIView!
    var bookedStatusText = "" // TO use when in full view
    var textToShowOnBooked = "" // TO use when collapsed
    override func viewDidLoad() {
        super.viewDidLoad()
        trackTavleVIew.delegate = self
        trackTavleVIew.dataSource = self
        gifViewArray = [gifOne , gifTwo ,gifThree , gifFour]
        roundImageArray = [imageOne , imageTwo , imageThree , imageFour]
        mapView.delegate = self
      //  let delegate = customMapVIewDelegate()
        mapView.isZoomEnabled = true
       // mapView.delegate = delegate

        trackTavleVIew.register(UINib(nibName: reuseNibIdentifier.trackDateCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.trackDateCell)
        trackTavleVIew.register(UINib(nibName: reuseNibIdentifier.trackDataCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.trackDataCell)
        trackTavleVIew.separatorColor = .clear
        trackTavleVIew.backgroundColor = .clear
        trackTavleVIew.isOpaque = true
        trackTavleVIew.backgroundView = nil
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(collapseTracking))
        collapeseView.addGestureRecognizer(gesture2)
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(collapseTrackingMapVIew))
        mapCollapseView.addGestureRecognizer(gesture3)
        setupLabel()
        setUpGestures()
        setupMapView()
            setupStatus()
        collapseTracking(firstTIme: true)    // dont change the order of calling these functions
      //  travelHistoryView.isHidden = true
        let docGif = try! UIImage(gifName: "doc.gif")
        self.documentImageView.setGifImage(docGif, loopCount: -1)
        let podGif = try! UIImage(gifName: "pod.gif")
        self.podImageVIew.setGifImage(podGif, loopCount: -1)
        let loggedUsername = UserDefaults.standard.string(forKey: "\(userdefaultsKey.isLoggedIN)")
            if loggedUsername == "TRUE" {
                bottomButtonsView.isHidden = false
            }
        else
            {
             
                bottomButtonsView.isHidden = true
              
                
            }
        bottomButtonsView.layer.shadowColor = UIColor.black.cgColor
        bottomButtonsView.layer.shadowOpacity = 1
        bottomButtonsView.layer.shadowOffset = .zero
        bottomButtonsView.layer.shadowRadius = 10
        let bookingFrom = "Booking From"
        let companyName = trackingData?.billtoName?.uppercased()
        let label = UILabel()
            label.backgroundColor = .clear
            label.numberOfLines = 2
            label.textAlignment = .center
            label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        var titleString = ""
        
        
            titleString = "\(bookingFrom.uppercased())" + " \n" + "\(companyName ?? "")"

        
        let amountText = NSMutableAttributedString.init(string: titleString)

        // set the custom font and color for the 0,1 range in string
        amountText.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                  NSAttributedString.Key.foregroundColor: UIColor.black],
                                 range: NSMakeRange(0, bookingFrom.count))
        label.attributedText = amountText
            self.navigationItem.titleView = label
    }
    func setupStatus()
    {
        gifOne.isHidden = true
        var transportTypeGifImage = "truck.gif" // default if none match
        switch trackingData?.modeType {
        case "Air":
            transportTypeGifImage = "air.gif"
        case "Road":
            transportTypeGifImage = "truck.gif"
        case "Hand Carry":
            transportTypeGifImage = "handcarry.gif"
        case "Sea":
            transportTypeGifImage = "sea.gif"
        case "Courier":
            transportTypeGifImage = "courier.gif"
        case "Hot Shot":
            transportTypeGifImage = "hotshot.gif"
        default:
            print("err")
        }
        let gif = try! UIImage(gifName: transportTypeGifImage)
        self.gifThree.setGifImage(gif, loopCount: -1) // Will loop forever
        self.gifOne.setGifImage(gif, loopCount: -1) // Will loop forever
        self.gifTwo.setGifImage(gif, loopCount: -1) // Will loop forever
        self.gifFour.setGifImage(gif, loopCount: -1) // Will loop forever
        self.imageViewForGIf.setGifImage(gif, loopCount: -1) // Will loop forever
   //     let fromGif = try! UIImage(gifName: "loading.gif")
     //   self.fromImage.setGifImage(fromGif, loopCount: -1)
   //     let toGif = try! UIImage(gifName: "unloading.gif")
   //     self.toImage.setGifImage(toGif, loopCount: -1)
        //0 - booked , 1-pickedup , 2-intransit , 3-delivered
     
        switch trackingData?.trackStatus?.uppercased() {
        case "BOOKED":
            jobStatus = 0
            firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            secondStatusImage.tintColor = .lightGray
            thirdStatusImage.tintColor = .lightGray
            fourthStatusImage.tintColor = .lightGray
        case "COLLECTING" , "PICKED UP":
            jobStatus = 1
            firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            thirdStatusImage.tintColor = .lightGray
            fourthStatusImage.tintColor = .lightGray
        case "IN TRANSIT":
            jobStatus = 2
            firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            thirdStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            fourthStatusImage.tintColor = .lightGray
        case "DELIVERED":
            jobStatus = 3
            firstStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            secondStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            thirdStatusImage.tintColor = ConstantsUsedInProject.projectDarkGreen
            fourthStatusImage.image = UIImage(named: "last_complete")
        default:
            print(trackingData?.trackStatus as Any)
            print("status errro")
        }
        setUpGifs()
    }
    func setUpGifs()
    {
      
        let checkedImage = UIImage(named: "circlechecked")
        let unCheckedImage = UIImage(named: "circleunchecked")

        //0 - booked , 1-pickedup , 2-intransit , 3-delivered
        for n in 0 ... 3
        {
            if jobStatus == n
            {
                gifViewArray[n].isHidden = false
                roundImageArray[n].isHidden = true
                for i in 0 ... n
                {
                    roundImageArray[i].image = checkedImage
                }
                for i in n ... 3
                {
                    roundImageArray[i].image = unCheckedImage

                }
            }
            else
            {
                gifViewArray[n].isHidden = true

            }
        }
    }
 
    func setupMapView()
    {
        
        var coordinates = [CLLocationCoordinate2D]()
        if let safeData = trackingData
        {
            var from : CLLocationCoordinate2D?
            var to : CLLocationCoordinate2D?
          
            if safeData.consigneLatitude != nil && safeData.consigneLatitude != ""
            {
                let lat : CLLocationDegrees = Double(safeData.consigneLatitude!)!
                let long : CLLocationDegrees = Double(safeData.consigneLongitude!)!
                
                let cons  = CLLocationCoordinate2D(latitude: lat, longitude: long)
                from = cons
                coordinates.append(cons)
            }
            if safeData.shipperLatitude != nil && safeData.shipperLatitude != ""
            {
                let lat : CLLocationDegrees = Double(safeData.shipperLatitude!)!
                let long : CLLocationDegrees = Double(safeData.shipperLongitude!)!

                let ship  = CLLocationCoordinate2D(latitude: lat, longitude: long)
                coordinates.append(ship)
                to = ship
             
            }
         //   let la  = (Double(safeData.consigneLatitude!)! + Double(safeData.shipperLatitude!)!) / 2
          //  let lo = (Double(safeData.consigneLongitude!)! + Double(safeData.shipperLongitude!)!) / 2
          //  let x  = CLLocationCoordinate2D(latitude: la, longitude: lo)
//            coordinates.append(x)
    //    let x = geographicMidpoint(betweenCoordinates: coordinates)
           // coordinates.append(x)
            let annotionFrom = CustomPointAnnotation()
            annotionFrom.coordinate = to!
            //     annotionFrom.title = "annotionFrom"
            //     annotionFrom.subtitle = "Subtitle"
                 annotionFrom.imageName = "fmark1"
            let annotionTo = CustomPointAnnotation()
            annotionTo.coordinate = from!
             //    annotionTo.title = "annotionTo"
             //    annotionTo.subtitle = "Subtitle"
                 annotionTo.imageName = "fmark2"

                    // if mode type oad , road
                 mapView.addAnnotation(annotionTo)
            mapView.addAnnotation(annotionFrom)

        /*   for location in coordinates {
                       let annotation = MKPointAnnotation()
                       annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                       mapView.addAnnotation(annotation)
            } */
           let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
            mapView.addOverlay(polyline)
            if let first = mapView.overlays.first {
                let rect = mapView.overlays.reduce(first.boundingMapRect, {$0.union($1.boundingMapRect)})
                mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                
            }
               addOverlays(from: from!, to: to!)
            let coordinate₀ = CLLocation(latitude: from!.latitude, longitude: from!.longitude)
            let coordinate₁ = CLLocation(latitude: to!.latitude, longitude: to!.longitude)

            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
            let distanceInKM = distanceInMeters / 1000
            //print(distanceInKM)\
            var centreCor = CLLocationCoordinate2D(latitude: from!.latitude + 20, longitude: from!.longitude + 20)
            if distanceInKM > 8000.0
            {
                  mapView.setCenter(from!, animated: true)

              //  mapView.setCenter(centreCor, animated: true)
                mapView.layoutMargins = UIEdgeInsets(top: 0.0, left: 300.0, bottom: 0.0, right: 0.0)


            }
           

        }
    
 

    }
    private func addOverlays(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) {
        var pointA = CLLocationCoordinate2DMake(41.375024, 2.149118) // Plaça d'Espanya, Barcelona
           var pointB = CLLocationCoordinate2DMake(41.380994, 2.185771) // Plaça Pau Vila, 1, Barcelona
        pointA = from
        pointB = to
        let line = LineOverlay(origin: pointA, destination: pointB)
        //   mapView.addOverlay(line)
           let arc = ArcOverlay(origin: pointA, destination: pointB,
                                style: LineOverlayStyle(strokeColor: .red, lineWidth: 4, alpha: 1))
           arc.radiusMultiplier = 0.5
    
           mapView.addOverlay(arc)
        
     //   let rect = arc.boundsMapRect
      //  mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    
       }
   

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {


            if !(annotation is CustomPointAnnotation) {
                return nil
            }

            let reuseId = "test"

        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if anView == nil {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                anView?.canShowCallout = true
            }
            else {
                anView!.annotation = annotation
            }

            //Set annotation-specific properties **AFTER**
            //the view is dequeued or created...

        let cpa = annotation as! CustomPointAnnotation
        anView!.image = UIImage(named:"\(cpa.imageName!)")
        anView?.cornerRadius = (anView?.frame.width)! / 2
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: (anView?.frame.width)! + 20.0, height: (anView?.frame.height)! + 20.0))
        backView.backgroundColor = .white
        backView.layer.cornerRadius = backView.frame.height / 2
        anView?.backgroundColor = .white
        //anView?.addSubview(backView)
        //view.insertSubview(backView, belowSubview: anView!)
 //       anView?.frame = CGRect(x: 0, y: 0, width: (anView?.frame.width)! + 20.0, height: (anView?.frame.height)! + 20.0)

            return anView
        }
    
  
    func mapView(_: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

          if let lineOverlay = overlay as? LineOverlay {
           

              return MapLineOverlayRenderer(lineOverlay)
          }
       // mapView.makeDashedBorderLine()
          return MKOverlayRenderer(overlay: overlay)
      }
  
  

    func setUpGestures()
    {
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(podPressed))
        podView.addGestureRecognizer(gesture1)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(documentPressed))
        documentView.addGestureRecognizer(gesture2)
    }
    @objc func podPressed()
    {
        if trackingData != nil
        {
            if trackingData?.podImage != ""
           {
            performSegue(withIdentifier: segueIdentifiers.trackingToPdfViewerpod, sender: nil)

           }
            else
           {
            let alert = UIAlertController(title: "POD", message: "No file found", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
           }
        }

    }
    @objc func documentPressed()
    {
        if trackingData != nil
        {
            if trackingData?.podImage != ""
           {
            performSegue(withIdentifier: segueIdentifiers.trackingToPdfViewerdocument, sender: nil)

           }
            else
           {
            let alert = UIAlertController(title: "Document", message: "No file found", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
           }
        }

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.trackingToPdfViewerpod
        {
          
            let vc = segue.destination as! PdfViewerController
            if trackingData != nil
            {
                var x = trackingData?.podImage
                x = "pod/\(x!)" // for pdfpath
                vc.pdfPath = x!
            }
        }
        if segue.identifier == segueIdentifiers.trackingToPdfViewerdocument
        {
            let vc = segue.destination as! PdfViewerController
            if trackingData != nil
            {
                var x = trackingData?.documents
                x = "document/\(x!)" // for document path

                vc.pdfPath = x!
            }
        }
        
    }
    func convertTimeFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
        return  dateFormatter.string(from: date!)
        
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    func setupLabel()
    {
        if let x = trackingData
        {
            arrivingByDate.text = convertDateFormater(x.jobEtaDate!)
            fromAddress.text = x.shipperLocationaddress?.uppercased()
            toAddress.text = x.consigneeLocationaddress?.uppercased()
            fromName.text = x.shipperName?.uppercased()
            toName.text = x.consigneeName?.uppercased()
            trackingNumber.text = x.docketNumber?.uppercased()
            weight.text = "\(x.weight ?? "") \(x.weighttypeName ?? "")"
            noOfPackages.text = "\(x.noofPackage ?? "") \(x.typeofPackage ?? "")"
            shipmentType.text = x.transportMethodType?.uppercased()
            specialHandlingSection.text = x.specialCargo?.uppercased()
            additionalServices.text = x.additionalService?.uppercased()
            signatureServices.text = "SIGNATURE REQUIRED"
            tersmOfDelivery.text = x.termOfDelivery?.uppercased()
            cargoDelivery.text = x.cargoDesc?.uppercased()
            additionalInfo.text = x.additionalInfo?.uppercased()
            shipmentTypeUNderGIf.text = x.transportMethodType
            statsuLabel.text = x.trackStatus
            jobNumber.text = x.jobNumber?.uppercased()
            referenceNumber.text = x.clientReferenceNo?.uppercased()
            if let y = x.tracking
            {
                print("A")
                if y.count != 0
                {
                    print("B")

                    for track in y
                    {
                        print("C")

                        let location = track.location
                        let time = convertTimeFormater(track.time!)
                        let date = convertDateFormater(track.date!)
                        let textToShow = "\(date) \(time), \(location ?? "")"
                        switch track.status?.uppercased(){
                        case "BOOKED":
                            bookedStatusText = textToShow
                            bookedStatusLabel.text = textToShow
                        case "COLLECTING" , "PICKED UP":
                            pickedUpStatusLabel.text = textToShow
                        case "IN TRANSIT":
                           inTransitStatusLabel.text = textToShow
                        case "DELIVERED":
                          deliveredStatusLabel.text = textToShow
                        default:
                            print(trackingData?.trackStatus as Any)
                            print("status errro")
                        }
                        if track.status?.uppercased() == trackingData?.trackStatus?.uppercased()
                        {
                            textToShowOnBooked = textToShow
                        }
                    }
                }
            }
            self.view.layoutIfNeeded()
            viewToDrawDottedLine.addDashedLine()
           
        //    dottedImageView.image = UIImage(named: "dotted-barline")

        }
    }
    @objc func collapseTrackingMapVIew()
    {
        if isCollapsedMapView
        {
            //247
            rotateMapview(by: .pi)
           
            view.layoutIfNeeded()
            mapViewHeight.constant = 200

            UIView.animate(withDuration: 1.0, animations: {
                 self.view.layoutIfNeeded()
            })
            
        }
        else
        {
            rotateMapview(by: -π)
            //self.rotate(by: -.pi)

            view.layoutIfNeeded()
            mapViewHeight.constant = 50

          
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
            } completion: { [self] _ in
                
            }

        }
        isCollapsedMapView.toggle()
    }
    @objc func collapseTracking(firstTIme : Bool)
    {
        var statusText = ""
        switch jobStatus {
        case 0:
            statusText = "Order Placed"
        case 1:
            statusText = "Picked Up"
        case 2:
            statusText = "In Transit"
        case 3:
            statusText = "Delivered"
            
        default:
            print("error in collapse swithc")
        }
            
        if isCollapsed
        {
            //247
            rotate(by: .pi)
            dottedImageONe.isHidden = false
            view.layoutIfNeeded()
            trackingVIewHeight.constant = 247

            UIView.animate(withDuration: 1.0, animations: {
                 self.view.layoutIfNeeded()
            })
            statusLabelFirst.text = "Order Placed"
            bookedStatusLabel.text = bookedStatusText
            if jobStatus != 0 //if the status is booked , the first gif is shown
            {
                gifOne.isHidden = true

            }
            roundImageArray[0].isHidden = false
        }
        else
        {
            rotate(by: -π)
            //self.rotate(by: -.pi)

            view.layoutIfNeeded()
            trackingVIewHeight.constant = 60

          
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
            } completion: { [self] _ in
                dottedImageONe.isHidden = true
                statusLabelFirst.text = trackingData?.trackStatus
                bookedStatusLabel.text = textToShowOnBooked
                gifOne.isHidden = false
                roundImageArray[0].isHidden = true
                

            }

        }
        isCollapsed.toggle()
    }
    var viewAngle: CGFloat = 0    // Right-side up to start
    let π = CGFloat.pi   // Swift allows special characters hold alt+p to use this, it allows for cleaner code
    /// Rotate trackingview icon
    /// - parameter by :  Angle to rotate
    
    func rotate(by angle: CGFloat) {

        for i in 0 ..< 4 {
            UIView.animate(withDuration: 0.4, delay: 0.125 * Double(i), options: .curveLinear, animations: {
                print(self.viewAngle)
                self.viewAngle += angle/4
                self.collapeseView.transform = CGAffineTransform(rotationAngle: self.viewAngle)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }

    }
    var viewAngleMapVIew: CGFloat = 0    // Right-side up to start
    func rotateMapview(by angle: CGFloat) {

        for i in 0 ..< 4 {
            UIView.animate(withDuration: 0.4, delay: 0.125 * Double(i), options: .curveLinear, animations: {
                print(self.viewAngleMapVIew)
                self.viewAngleMapVIew += angle/4
                self.mapCollapseView.transform = CGAffineTransform(rotationAngle: self.viewAngleMapVIew)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }

    }

}

extension TrackViewController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       /* let cell = trackTavleVIew.dequeueReusableCell(withIdentifier: reuseCellIdentifier.trackDateCell) as! TrackDateCell
        
        if let x = trackingData?.jobList?[indexPath.row]
        {
            cell.dateLabel.text = x.updatedOn
            cell.travelHistory = x.travelHistory
        }
        return cell */

        let cell = trackTavleVIew.dequeueReusableCell(withIdentifier: reuseCellIdentifier.trackDataCell) as! TrackDataCell
        if let x = trackingData?.travelHistory?[indexPath.row]
        {
            cell.statusLabel.text = x.status
            let location = x.location
            let time = convertTimeFormater(x.time!)
            let date = convertDateFormater(x.date!)
            let textToShow = "\(date) \(time), \(location ?? "")"
            cell.timeLabel.text = textToShow
            if indexPath.row + 1 == trackingData?.travelHistory?.count
            {
                cell.dottedImageVIew.isHidden = true
            }
           // cell.statusLabel.text = "\(indexPath.row)"
           //  cell.timeLabel.text = "\(indexPath.row)"
        }
       // print(x)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
   /*     //36
        //80
        //116
        //142
       
            let x = 36
        let c = trackingData?.jobList?[indexPath.row].travelHistory?.count ?? 0
            let h = (c * 80) + x
            print("height \(indexPath.row)     \(h)")
            print(c)
            return CGFloat(h) */
        return 65
       

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
     //   return trackingData?.jobList?.count ?? 0
        if trackingData != nil
       {

            let c =  trackingData?.travelHistory?.count
        let h = (c! * 65)

        tableViewHeight.constant = CGFloat(h)
       }
        else
        {
            tableViewHeight.constant = .zero
        }
        return trackingData?.travelHistory?.count ?? 0
    }

    
    
}



class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}




enum jobType {
    case booked
    case intransit
    case delivered
    case pickedUp
        
        }

extension UIView {

    func addDashedLine() {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = ConstantsUsedInProject.projectDarkGreen.cgColor
        shapeLayer.lineWidth = 4
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [0,8]
        shapeLayer.lineCap = .round
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: center.x, y: 0),
                                CGPoint(x: center.x, y: self.frame.height)])
        
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}

