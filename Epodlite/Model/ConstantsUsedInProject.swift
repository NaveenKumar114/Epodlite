//
//  ConstantsUsedInProject.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-01.
//

import Foundation
import UIKit
struct ConstantsUsedInProject {
    static let appThemeColor : UIColor = #colorLiteral(red: 0.1647058824, green: 0.5450980392, blue: 0, alpha: 1)
    static let appThemeColorName = "AccentColor"
    static let baseUrl = "https://thefollo.com/epodlite/api/"
    static let baseImgUrl = "https://www.thefollo.com/epodlite/uploads/"
    static let baseDocumentUrl = "https://www.thefollo.com/epodlite/uploads/"

    static let projectDarkGreen : UIColor = #colorLiteral(red: 0.1647058824, green: 0.5450980392, blue: 0, alpha: 1)
    static let projectLightGreen : UIColor = #colorLiteral(red: 0.9019607843, green: 0.9450980392, blue: 0.8901960784, alpha: 1)
    static let dashboardBackground : UIColor = #colorLiteral(red: 0.9647058824, green: 0.9607843137, blue: 0.9764705882, alpha: 1)

    //    static let baseUrl = "https://www.thefollo.com/epodlite_demo/uploads/"
  //  static let baseUrl = "https://www.thefollo.com/epodlite/uploads/"


}

struct userdefaultsKey {
  
    static let cliendid = "clientid"
    static let  isLoggedIN = "isLoggedIn"
    static let  clientName = "clientName"

    static let  subscriberName = "subscriberName"
    static let  subscriberImg = "subscriberImg"


 
}
struct notificationName
{
    static let mapCenter = "mapCenter"
}
