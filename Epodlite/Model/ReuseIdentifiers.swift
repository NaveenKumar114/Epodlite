//
//  ReuseIdentifiers.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-06-30.
//

import Foundation

struct reuseCellIdentifier
{
    static var trackDataCell  = "TrackDataCell"
    static var trackDateCell  = "TrackDateCell"
    static var jobListCell = "JobListCell"
}
struct reuseNibIdentifier
{
    static var trackDataCell  = "TrackDataCell"
    static var trackDateCell  = "TrackDateCell"
    static var jobListCell = "JobListCell"

}
struct segueIdentifiers {
    static var dashboardToTracking = "dashboardToTracking"
    static var dashboardToDocketScanner = "dashboardToDocketScanning"
    static var trackingToPdfViewerpod = "trackingToPdfViewerpod"
    static var trackingToPdfViewerdocument = "trackingToPdfViewerdocu"

}
