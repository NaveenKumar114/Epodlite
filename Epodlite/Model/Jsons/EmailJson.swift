//
//  EmailJson.swift
//  Epodlite
//
//  Created by Naveen Natrajan on 2021-07-01.
//

import Foundation

// MARK: - EmailJSON
struct EmailJSON: Codable {
    let code: Int?
    let response: String?
    let client: Client?
    let subscriber: Subscriber?
}

// MARK: - Client
struct Client: Codable {
    let clientID, userID, clientName, contactPerson: String?
    let contactPhone, email, address, locationAddress: String?
    let displayName, password, shipperid, consigneeid: String?
    let billtoID, isActive, referid, latitude: String?
    let longitude, createdOn, updatedOn, createdBy: String?
    let updatedBy, tokenidIos, tokenid: String?

    enum CodingKeys: String, CodingKey {
        case clientID = "client_id"
        case userID = "user_id"
        case clientName = "client_name"
        case contactPerson = "contact_person"
        case contactPhone = "contact_phone"
        case email, address
        case locationAddress = "location_address"
        case displayName = "display_name"
        case password, shipperid, consigneeid
        case billtoID = "billto_id"
        case isActive = "is_active"
        case referid, latitude, longitude
        case createdOn = "created_on"
        case updatedOn = "updated_on"
        case createdBy, updatedBy
        case tokenidIos = "tokenid_ios"
        case tokenid
    }
}

// MARK: - Subscriber
struct Subscriber: Codable {
    let userid, companyName, contactPerson, username: String?
    let profileImage, email, password, mobile: String?
    let address: String?
    let identificationNo, identificationType: String?
    let role, roleName, images, status: String?
    let planExpire, planID, currentPlanID, planName: String?
    let terms, privacy, forgottenCode, freeTrialStatus: String?
    let subscriberID, createdBy, createdDate, modifyBy: String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case userid
        case companyName = "company_name"
        case contactPerson = "contact_person"
        case username
        case profileImage = "profile_image"
        case email, password, mobile, address
        case identificationNo = "identification_no"
        case identificationType = "identification_type"
        case role
        case roleName = "role_name"
        case images, status
        case planExpire = "plan_expire"
        case planID = "plan_id"
        case currentPlanID = "current_plan_id"
        case planName = "plan_name"
        case terms, privacy
        case forgottenCode = "forgotten_code"
        case freeTrialStatus = "free_trial_status"
        case subscriberID = "subscriber_id"
        case createdBy = "created_by"
        case createdDate = "created_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
