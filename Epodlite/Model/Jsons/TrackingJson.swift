// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let trackingJSON = try? newJSONDecoder().decode(TrackingJSON.self, from: jsonData)

import Foundation

// MARK: - TrackingJSON
struct TrackingJSON: Codable {
    let code: Int?
    let response: String?
    let jobList: [JobList]?
    let jobCounts: JobCounts?

    enum CodingKeys: String, CodingKey {
        case code, response
        case jobList = "job_list"
        case jobCounts = "job_counts"
    }
}

// MARK: - JobCounts
struct JobCounts: Codable {
    let allCount, bookedCount, collectingCount, transitCount: String?
    let deliveredCount: String?

    enum CodingKeys: String, CodingKey {
        case allCount = "all_count"
        case bookedCount = "booked_count"
        case collectingCount = "collecting_count"
        case transitCount = "transit_count"
        case deliveredCount = "delivered_count"
    }
}

// MARK: - JobList
struct JobList: Codable {
    let jobIDPrimary, userID, loginUserID, createdBy: String?
    let sequence, modeType, jobNumber, clientReferenceNo: String?
    let docketNumber, transportMethodType, shipperName, shipperID: String?
    let shipperContactperson, shipperMobile, shipperLocationaddress, shipperEmail: String?
    let shipperDisplayname, shipperLatitude, shipperLongitude, consigneeName: String?
    let consigneeID, consigneeContactperson, consigneeMobile, consigneeLocationaddress: String?
    let consigneeEmail, consigneeDisplayname, consigneLatitude, consigneLongitude: String?
    let billtoName, billtoID, mainbranchBillto, billtoContactperson: String?
    let billtoMobile, billtoLocationaddress, billToemail, billDisplayname: String?
    let billtoLatitude, billtoLongitude, jobEtdDate, jobEtaDate: String?
    let cargoDesc, weight, weighttypeID, weighttypeName: String?
    let noofPackage, volumedims, typeofPackage, packageID: String?
    let termOfDelivery, termOfDeliveryID, additionalInfo, specialCargo: String?
    let cargoid, additionalService, serviceID, freightCharge: String?
    let documents, openpdf, podImage, jobStatus: String?
    let trackStatus, openFlag, openName, openID: String?
    let createdOn, updatedOn, updatedBy: String?
    let travelHistory: [TravelHistory]?
    let tracking: [Tracking]?

    enum CodingKeys: String, CodingKey {
        case jobIDPrimary = "job_id_primary"
        case userID = "user_id"
        case loginUserID = "login_user_id"
        case createdBy, sequence
        case modeType = "mode_type"
        case jobNumber = "job_number"
        case clientReferenceNo = "client_reference_no"
        case docketNumber = "docket_number"
        case transportMethodType = "transport_method_type"
        case shipperName = "shipper_name"
        case shipperID = "shipper_id"
        case shipperContactperson = "shipper_contactperson"
        case shipperMobile = "shipper_mobile"
        case shipperLocationaddress = "shipper_locationaddress"
        case shipperEmail = "shipper_email"
        case shipperDisplayname = "shipper_displayname"
        case shipperLatitude = "shipper_latitude"
        case shipperLongitude = "shipper_longitude"
        case consigneeName = "consignee_name"
        case consigneeID = "consignee_id"
        case consigneeContactperson = "consignee_contactperson"
        case consigneeMobile = "consignee_mobile"
        case consigneeLocationaddress = "consignee_locationaddress"
        case consigneeEmail = "consignee_email"
        case consigneeDisplayname = "consignee_displayname"
        case consigneLatitude = "consigne_latitude"
        case consigneLongitude = "consigne_longitude"
        case billtoName = "billto_name"
        case billtoID = "billto_id"
        case mainbranchBillto = "mainbranch_billto"
        case billtoContactperson = "billto_contactperson"
        case billtoMobile = "billto_mobile"
        case billtoLocationaddress = "billto_locationaddress"
        case billToemail = "bill_toemail"
        case billDisplayname = "bill_displayname"
        case billtoLatitude = "billto_latitude"
        case billtoLongitude = "billto_longitude"
        case jobEtdDate = "job_etd_date"
        case jobEtaDate = "job_eta_date"
        case cargoDesc = "cargo_desc"
        case weight
        case weighttypeID = "weighttype_id"
        case weighttypeName = "weighttype_name"
        case noofPackage = "noof_package"
        case volumedims
        case typeofPackage = "typeof_package"
        case packageID = "package_id"
        case termOfDelivery = "term_of_delivery"
        case termOfDeliveryID = "term_of_deliveryId"
        case additionalInfo = "additional_info"
        case specialCargo = "special_cargo"
        case cargoid
        case additionalService = "additional_service"
        case serviceID = "service_id"
        case freightCharge = "freight_charge"
        case documents, openpdf
        case podImage = "pod_image"
        case jobStatus = "job_status"
        case trackStatus = "track_status"
        case openFlag = "open_flag"
        case openName = "open_name"
        case openID = "open_id"
        case createdOn = "created_on"
        case updatedOn = "updated_on"
        case updatedBy
        case travelHistory = "travel_history"
        case tracking
    }
}

// MARK: - Tracking
struct Tracking: Codable {
    let status, location, date, time: String?
}

// MARK: - TravelHistory
struct TravelHistory: Codable {
    let travelHistoryID, activityID, trackingNo, date: String?
    let time, activity, location, status: String?
    let createby, modifyby, createDate, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case travelHistoryID = "travel_history_id"
        case activityID = "activity_id"
        case trackingNo = "tracking_no"
        case date, time, activity, location, status, createby, modifyby
        case createDate = "create_date"
        case modifyDate = "modify_date"
    }
}
